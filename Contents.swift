import UIKit

var myMainView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 500, height: 500))
myMainView.backgroundColor = UIColor.init(red: 11, green: 56, blue: 0, alpha: 1)

func logic(_ x1: Int, _ y1: Int, _ x2: Int, _ y2: Int) -> (Int,Int,Int,Int){
    
   var (delt,tempY,tempX,temp) = (0,0,0,1)
  
    if x2 - x1 != 0 {
        (delt, tempX) = (x2 - x1, 1)
    }
    if y2 - y1 != 0 {
        (delt, tempY) = (y2 - y1, 1)
    }
    if (x2 - x1 != 0) && (y2 - y1 != 0) {
        (delt, tempY) = (y2 - y1, 1)
    }
    if (y1 > y2) {
        (delt,temp) = (y1 - y2,  -1)
    }
    if (y2 > y1) {
        (delt,temp) = (y2 - y1, 1)
    }

    return (delt,tempY,tempX,temp)
}

func lineDrawingAnt(x1: Int, y1: Int, x2: Int, y2: Int, color: UIColor = UIColor.black,viewForDisplaying: UIView){
    var arrRect = [UIView]()
    
    if (x1 > Int(viewForDisplaying.bounds.width)) || (x2 > Int(viewForDisplaying.bounds.width)) || (y1 > Int(viewForDisplaying.bounds.height)) || (y2 > Int(viewForDisplaying.bounds.height)){ print("I can't draw it!!")}
    else {
   let (delta,tempY,tempX,temp) = logic(x1,y1,x2,y2)
    for i in 0...delta {
        arrRect.append(UIView.init(frame: CGRect.init(x: x1 + i * tempX, y: y1 + i * temp * tempY, width: 2, height: 2)))
        arrRect[i].backgroundColor = color
        viewForDisplaying.addSubview(arrRect[i])
        }
    }
//    for i in 0...delta {
//        arrRect.append(UIView.init(frame: CGRect.init(x: x1 + i * tempX, y: y1 + i * temp * tempY, width: 2, height: 2)))
//        arrRect[i].backgroundColor = color
//        viewForDisplaying.addSubview(arrRect[i])
//    }
}

////Roof
//lineDrawingAnt(x1: 250, y1: 50, x2:  350, y2:200, color: UIColor.black,viewForDisplaying: myMainView)
//lineDrawingAnt(x1: 100, y1: 200, x2: 250, y2:50, color: UIColor.black,viewForDisplaying: myMainView)
//lineDrawingAnt(x1: 100, y1: 200, x2: 400, y2:200, color: UIColor.black,viewForDisplaying: myMainView)
////Walls
//lineDrawingAnt(x1: 100, y1: 200, x2: 100, y2:350, color: UIColor.black,viewForDisplaying: myMainView)
//lineDrawingAnt(x1: 400, y1: 200, x2: 400, y2:350, color: UIColor.black,viewForDisplaying: myMainView)
////Bottom
//lineDrawingAnt(x1: 100, y1: 350, x2: 400, y2:350, color: UIColor.black,viewForDisplaying: myMainView)
////Door
//lineDrawingAnt(x1: 100, y1: 250, x2: 150, y2:250, color: UIColor.black,viewForDisplaying: myMainView)
//lineDrawingAnt(x1: 150, y1: 250, x2: 150, y2:350, color: UIColor.black,viewForDisplaying: myMainView)
//
////Window
//lineDrawingAnt(x1: 250, y1: 300, x2: 300, y2:300, color: UIColor.black,viewForDisplaying: myMainView)
//
//lineDrawingAnt(x1: 250, y1: 300, x2: 250, y2:250, color: UIColor.black,viewForDisplaying: myMainView)
//
//lineDrawingAnt(x1: 300, y1: 300, x2: 300, y2:250, color: UIColor.black,viewForDisplaying: myMainView)
//
//lineDrawingAnt(x1: 250, y1: 250, x2: 300, y2:250, color: UIColor.black,viewForDisplaying: myMainView)
//
//lineDrawingAnt(x1: 275, y1: 250, x2: 275, y2:300, color: UIColor.black,viewForDisplaying: myMainView)
//
//lineDrawingAnt(x1: 250, y1: 275, x2: 300, y2:275, color: UIColor.black,viewForDisplaying: myMainView)
//
//myMainView
